# Create a library called "names" which includes the source file "*.cxx".
# The extension is already found. Any number of sources could be listed here.
file(GLOB STRINGS_FILES *.cpp)
add_library (names STATIC ${STRINGS_FILES})

# Includes only for this target.
target_include_directories (names PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

