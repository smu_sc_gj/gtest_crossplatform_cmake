#include <limits.h>
#include "gtest/gtest.h"

#include "NameListTestHarness.h"
#include "NameList.h"

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

NameListTestHarness::NameListTestHarness()
{
    this->nameList = nullptr;
}

NameListTestHarness::~NameListTestHarness()
{
    this->nameList.reset();
}

void NameListTestHarness::SetUp()
{
    this->nameList = shared_ptr<NameList>(new NameList());
}

void NameListTestHarness::TearDown()
{
    this->nameList.reset();

    //Test the point has been deallocated.

    this->nameList = nullptr;
}

int NameListTestHarness::fakeHelperMethod()
{
    //This is a fake, just to show how its done. 
    return 0;
}

TEST_F(NameListTestHarness,constructorTest)
{
    ASSERT_EQ(this->nameList->getSize(),0);

    ASSERT_EQ(this->nameList->begin(), this->nameList->end());
}

TEST_F(NameListTestHarness,itteratorTest)
{
    string name1("One");
    string name2("Two");
    string name3("Three");

    this->nameList->addName(name1);
    this->nameList->addName(name2);
    this->nameList->addName(name3);

    NameList::iterator itt = this->nameList->begin();

    ASSERT_EQ(*itt,name1);
    ++itt;
    ASSERT_EQ(*itt,name2);
    ++itt;
    ASSERT_EQ(*itt,name3);
}

TEST_F(NameListTestHarness,fillList)
{
    const int MAX_SIZE = 8;

    string name1("One");
    string name2("Two");
    string name3("Thee");

    string name4("Four");
    string name5("Five");
    string name6("Six");

    string name7("Seven");
    string name8("Eight");

    this->nameList->addName(name1);
    this->nameList->addName(name2);
    this->nameList->addName(name3);

    this->nameList->addName(name4);
    this->nameList->addName(name5);
    this->nameList->addName(name6);

    this->nameList->addName(name7);
    this->nameList->addName(name8);

    int fullList = this->nameList->getSize();

    ASSERT_EQ(fullList, MAX_SIZE);
}

TEST_F(NameListTestHarness,testPick)
{
    const int MAX_SIZE = 8;

    vector<string> picked;

    string name1("One");
    string name2("Two");
    string name3("Thee");

    string name4("Four");
    string name5("Five");
    string name6("Six");

    string name7("Seven");
    string name8("Eight");

    this->nameList->addName(name1);
    this->nameList->addName(name2);
    this->nameList->addName(name3);

    this->nameList->addName(name4);
    this->nameList->addName(name5);
    this->nameList->addName(name6);

    this->nameList->addName(name7);
    this->nameList->addName(name8);

    int fullList = this->nameList->getSize();

    string s;

    for(int i = 0; i < MAX_SIZE; i++)
    {
        this->nameList->pickName(s);
        picked.push_back(s);
    }

    int newListSize = picked.size();

    ASSERT_EQ(fullList,newListSize);

    int emptyList = this->nameList->getSize();

    ASSERT_EQ(emptyList,0);
}

TEST_F(NameListTestHarness,testLoad_sizeTest)
{
    const int CURRENT_SIZE = 9;

    this->nameList->loadFile(testFile);

    int listSize = this->nameList->getSize();

    ASSERT_EQ(listSize, CURRENT_SIZE);
}


TEST_F(NameListTestHarness,testLoad_namesCheck)
{
    bool diff = false;

    vector<string> testy;
    testy.push_back("Ron");
    testy.push_back("Harry");
    testy.push_back("Hermione");
    testy.push_back("Malfoy");
    testy.push_back("Neville");
    testy.push_back("Jinny");
    testy.push_back("Charlie");
    testy.push_back("Fred");
    testy.push_back("George");

    this->nameList->loadFile(testFile);

    int listSize = this->nameList->getSize();

    ASSERT_GT(listSize, 0);

    /*Bad test, works on the basis the order of the items are the same */

    NameList::iterator itt1 = this->nameList->begin();
    vector<string>::iterator itt2 = testy.begin();

    while(itt1 != this->nameList->end())
    {
        if((*itt1).compare(*itt2)!=0)
        {
            diff = true;
        }
        ++itt1;
        ++itt2;
    }
    ASSERT_EQ(diff,false);
}
