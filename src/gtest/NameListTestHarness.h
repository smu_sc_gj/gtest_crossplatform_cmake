#ifndef NAMELISTTESTHARNESS_H
#define NAMELISTTESTHARNESS_H

#include <limits.h>
#include <gtest/gtest.h>
#include <string>

#include <memory>

using std::shared_ptr;

class NameList;

class NameListTestHarness : public ::testing::Test
{
protected:
    virtual void SetUp();
    virtual void TearDown();

    shared_ptr<NameList> nameList;
    const std::string testFile = "testfiles/testFile.csv";

public:
    NameListTestHarness();
    virtual ~NameListTestHarness();

    int fakeHelperMethod();
};

#endif // CUBETESTHARNESS_H
