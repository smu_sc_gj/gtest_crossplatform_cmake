
# Add executable called "App" that is built from the source files
# "game.cxx" The extensions are automatically found.
add_executable (App main.cpp)


# Includes only for this target - these could possibly be for all targets but...
target_include_directories (App PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (App PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../names)

# Link the executable to the App library. Since the lib library has
# public include directories we will use those link directories when building

# App
target_link_libraries (App LINK_PUBLIC names)
